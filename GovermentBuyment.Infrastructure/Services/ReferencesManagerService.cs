using GovermentBuyment.RestParser.Handler;

namespace GovermentBuyment.Infrastructure.Services;

public class ReferencesManagerService
{
    private ExtractRefMonthsHandler _monthsHandler;
    private ExtractRefTradeMethodHandler _tradeMethodHandler;
    public ReferencesManagerService(ExtractRefMonthsHandler monthsHandler, ExtractRefTradeMethodHandler tradeMethodHandler)
    {
        _monthsHandler = monthsHandler;
        _tradeMethodHandler = tradeMethodHandler;
    }


    public async Task ExtractAllReferences()
    {
        _monthsHandler.SetNext(_tradeMethodHandler);
        await _monthsHandler.Handle(new {});
    }
    
}