﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GovermentBuyment.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class References : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "SystemId",
                schema: "public",
                table: "Plans",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "RefMonths",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SystemId = table.Column<long>(type: "bigint", nullable: false),
                    Name_Ru = table.Column<string>(type: "text", nullable: true),
                    Name_Kk = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefMonths", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RefTradeMethods",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    SymbolCode = table.Column<string>(type: "text", nullable: false),
                    SystemId = table.Column<long>(type: "bigint", nullable: false),
                    Name_Ru = table.Column<string>(type: "text", nullable: true),
                    Name_Kk = table.Column<string>(type: "text", nullable: true),
                    Code = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefTradeMethods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RefUnits",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AlphaCode = table.Column<string>(type: "text", nullable: false),
                    SystemId = table.Column<long>(type: "bigint", nullable: false),
                    Name_Ru = table.Column<string>(type: "text", nullable: true),
                    Name_Kk = table.Column<string>(type: "text", nullable: true),
                    Code = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefUnits", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RefMonths",
                schema: "public");

            migrationBuilder.DropTable(
                name: "RefTradeMethods",
                schema: "public");

            migrationBuilder.DropTable(
                name: "RefUnits",
                schema: "public");

            migrationBuilder.DropColumn(
                name: "SystemId",
                schema: "public",
                table: "Plans");
        }
    }
}
