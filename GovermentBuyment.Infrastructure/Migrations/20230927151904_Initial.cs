﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GovermentBuyment.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "Plans",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PlanId = table.Column<long>(type: "bigint", nullable: false),
                    PlanRootId = table.Column<long>(type: "bigint", nullable: false),
                    SystemSubjectId = table.Column<long>(type: "bigint", nullable: true),
                    SystemOrganizerId = table.Column<long>(type: "bigint", nullable: true),
                    SubjectBin = table.Column<string>(type: "text", nullable: true),
                    SubjectName_Ru = table.Column<string>(type: "text", nullable: true),
                    SubjectName_Kk = table.Column<string>(type: "text", nullable: true),
                    Name_Ru = table.Column<string>(type: "text", nullable: true),
                    Name_Kk = table.Column<string>(type: "text", nullable: true),
                    RefTradeMethodsId = table.Column<long>(type: "bigint", nullable: true),
                    RefUnitsCode = table.Column<long>(type: "bigint", nullable: true),
                    Count = table.Column<long>(type: "bigint", nullable: true),
                    Price = table.Column<double>(type: "double precision", nullable: true),
                    Amount = table.Column<double>(type: "double precision", nullable: true),
                    FinancialYear = table.Column<long>(type: "bigint", nullable: true),
                    DateCreate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    Description_Ru = table.Column<string>(type: "text", nullable: true),
                    Description_Kk = table.Column<string>(type: "text", nullable: true),
                    ExtraDescription_Ru = table.Column<string>(type: "text", nullable: true),
                    ExtraDescription_Kk = table.Column<string>(type: "text", nullable: true),
                    SumFirstYear = table.Column<double>(type: "double precision", nullable: true),
                    SumSecondYear = table.Column<double>(type: "double precision", nullable: true),
                    SumThirdYear = table.Column<double>(type: "double precision", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plans", x => x.Id);
                    table.UniqueConstraint("AK_Plans_PlanId", x => x.PlanId);
                });

            migrationBuilder.CreateTable(
                name: "PlanKatos",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PlanPointId = table.Column<long>(type: "bigint", nullable: false),
                    RefKatoCode = table.Column<string>(type: "text", nullable: true),
                    RefCountriesCode = table.Column<string>(type: "text", nullable: true),
                    FullDeliveryPlace_Ru = table.Column<string>(type: "text", nullable: true),
                    FullDeliveryPlace_Kk = table.Column<string>(type: "text", nullable: true),
                    Count = table.Column<double>(type: "double precision", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    SystemId = table.Column<int>(type: "integer", nullable: true),
                    IndexDate = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanKatos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlanKatos_Plans_PlanPointId",
                        column: x => x.PlanPointId,
                        principalSchema: "public",
                        principalTable: "Plans",
                        principalColumn: "PlanId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlanKatos_PlanPointId",
                schema: "public",
                table: "PlanKatos",
                column: "PlanPointId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlanKatos",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Plans",
                schema: "public");
        }
    }
}
