﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GovermentBuyment.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class ReferencesPlanFk : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "RefUnitsCode",
                schema: "public",
                table: "Plans",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "RefTradeMethodsId",
                schema: "public",
                table: "Plans",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RefMonthId",
                schema: "public",
                table: "Plans",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_RefUnits_SystemId",
                schema: "public",
                table: "RefUnits",
                column: "SystemId");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_RefTradeMethods_SystemId",
                schema: "public",
                table: "RefTradeMethods",
                column: "SystemId");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_RefMonths_SystemId",
                schema: "public",
                table: "RefMonths",
                column: "SystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Plans_RefMonthId",
                schema: "public",
                table: "Plans",
                column: "RefMonthId");

            migrationBuilder.CreateIndex(
                name: "IX_Plans_RefTradeMethodsId",
                schema: "public",
                table: "Plans",
                column: "RefTradeMethodsId");

            migrationBuilder.CreateIndex(
                name: "IX_Plans_RefUnitsCode",
                schema: "public",
                table: "Plans",
                column: "RefUnitsCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Plans_RefMonths_RefMonthId",
                schema: "public",
                table: "Plans",
                column: "RefMonthId",
                principalSchema: "public",
                principalTable: "RefMonths",
                principalColumn: "SystemId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Plans_RefTradeMethods_RefTradeMethodsId",
                schema: "public",
                table: "Plans",
                column: "RefTradeMethodsId",
                principalSchema: "public",
                principalTable: "RefTradeMethods",
                principalColumn: "SystemId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Plans_RefUnits_RefUnitsCode",
                schema: "public",
                table: "Plans",
                column: "RefUnitsCode",
                principalSchema: "public",
                principalTable: "RefUnits",
                principalColumn: "SystemId",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plans_RefMonths_RefMonthId",
                schema: "public",
                table: "Plans");

            migrationBuilder.DropForeignKey(
                name: "FK_Plans_RefTradeMethods_RefTradeMethodsId",
                schema: "public",
                table: "Plans");

            migrationBuilder.DropForeignKey(
                name: "FK_Plans_RefUnits_RefUnitsCode",
                schema: "public",
                table: "Plans");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_RefUnits_SystemId",
                schema: "public",
                table: "RefUnits");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_RefTradeMethods_SystemId",
                schema: "public",
                table: "RefTradeMethods");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_RefMonths_SystemId",
                schema: "public",
                table: "RefMonths");

            migrationBuilder.DropIndex(
                name: "IX_Plans_RefMonthId",
                schema: "public",
                table: "Plans");

            migrationBuilder.DropIndex(
                name: "IX_Plans_RefTradeMethodsId",
                schema: "public",
                table: "Plans");

            migrationBuilder.DropIndex(
                name: "IX_Plans_RefUnitsCode",
                schema: "public",
                table: "Plans");

            migrationBuilder.DropColumn(
                name: "RefMonthId",
                schema: "public",
                table: "Plans");

            migrationBuilder.AlterColumn<long>(
                name: "RefUnitsCode",
                schema: "public",
                table: "Plans",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "RefTradeMethodsId",
                schema: "public",
                table: "Plans",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");
        }
    }
}
