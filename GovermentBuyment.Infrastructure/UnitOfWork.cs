using GovermentBuyment.Domain.SeedWork;
using Microsoft.Extensions.DependencyInjection;

namespace GovermentBuyment.Infrastructure;

public class UnitOfWork : IUnitOfWork {
    private readonly AppDbContext _context;
    private readonly IServiceProvider _serviceProvider;

    public UnitOfWork(AppDbContext context, IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
        _context = context;
    }

    public ICrudRepository<TDomain> GetRepository<TDomain>()
        where TDomain : Entity, IAggregateRoot
    {
        return (_serviceProvider.GetRequiredService(typeof(ICrudRepository<TDomain>)) as ICrudRepository<TDomain>)!;
    }

    public Task SaveChangesAsync()
    {
        Console.WriteLine("SAVED");
        return _context.SaveChangesAsync();
    }
}