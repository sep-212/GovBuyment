﻿using System;
using GovermentBuyment.Domain.IRepositories;
using GovermentBuyment.Domain.SeedWork;
using GovermentBuyment.Infrastructure.Mapping;
using GovermentBuyment.Infrastructure.Repositories;
using GovermentBuyment.Infrastructure.Repositories.References;
using GovermentBuyment.Infrastructure.Services;
using GovermentBuyment.RestParser;
using GovermentBuyment.RestParser.API;
using GovermentBuyment.RestParser.Handler;
using GovermentBuyment.RestParser.Options;
using MGZ.ParserManager.Rest;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Refit;

namespace GovermentBuyment.Infrastructure;

public static class DiExtensions
{
    public static IServiceCollection AddCommonServices(this IServiceCollection services, IConfiguration config)
    {
        services.AddAutoMapper(typeof(PlanMapping).Assembly);
        services.AddScoped<AuthorizationReferencesMessageHandler>();
        services.AddScoped<IUnitOfWork, UnitOfWork>();

        #region Repositories

        services.AddScoped<IPlanRepository, PlanRepository>();
        services.AddScoped<IRefMonthRepository, RefMonthRepository>();
        services.AddScoped<IRefUnitRepository, RefUnitRepository>();
        services.AddScoped<IRefTradeMethodRepository, RefTradeMethodRepository>();

        #endregion
        
        #region APIs

        services.AddRefitClient<IReferencesApi>().ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler(){
                AllowAutoRedirect = false,
                ServerCertificateCustomValidationCallback = (message, cert, chain, sslErrors) => true
            })
            .ConfigureHttpClient(c =>{
                c.BaseAddress = new Uri(config["ReferencesApi:ApiEndpoint"] ?? string.Empty);
                c.Timeout = TimeSpan.FromMinutes(30);
            }).AddHttpMessageHandler<AuthorizationReferencesMessageHandler>();

        #endregion

        #region Handlers

        services.AddTransient<ExtractRefMonthsHandler>();
        services.AddTransient<ExtractRefTradeMethodHandler>();

        #endregion

        #region Services

        services.AddScoped<ReferencesManagerService>();

        #endregion
        
        return services;
    }

}