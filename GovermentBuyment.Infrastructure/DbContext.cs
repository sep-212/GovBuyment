﻿using GovermentBuyment.Domain.Entities;
using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Infrastructure.Configs;
using GovermentBuyment.Infrastructure.Configs.References;
using Microsoft.EntityFrameworkCore;

namespace GovermentBuyment.Infrastructure;


public class AppDbContext : DbContext
{
    public DbSet<PlanEntity> Plans { get; set; }
    public DbSet<PlanKatoEntity> PlanKatos { get; set; }
    public DbSet<RefMonthEntity> RefMonths { get; set; }
    public DbSet<RefTradeMethodEntity> RefTradeMethods { get; set; }
    public DbSet<RefUnitEntity> RefUnits { get; set; }

    public AppDbContext(DbContextOptions<AppDbContext> options)
       : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyConfiguration(new PlanConfig());
        modelBuilder.ApplyConfiguration(new PlanKatoConfig());
        modelBuilder.ApplyConfiguration(new RefMonthConfig());
        modelBuilder.ApplyConfiguration(new RefUnitConfig());
        modelBuilder.ApplyConfiguration(new RefTradeMethodConfig());
    }
}

