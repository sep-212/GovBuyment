using AutoMapper;
using GovermentBuyment.Domain.Entities;
using GovermentBuyment.Domain.IRepositories;

namespace GovermentBuyment.Infrastructure.Repositories;

public class PlanKatoRepository : BaseRepository<PlanKatoEntity>, IPlanKatoRepository
{
    public PlanKatoRepository(AppDbContext context, IMapper mapper) : base(context, mapper)
    {
    }
}