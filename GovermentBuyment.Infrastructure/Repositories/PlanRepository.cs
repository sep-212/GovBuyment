﻿using System;
using AutoMapper;
using GovermentBuyment.Domain.Entities;
using GovermentBuyment.Domain.IRepositories;

namespace GovermentBuyment.Infrastructure.Repositories;


public class PlanRepository : BaseRepository<PlanEntity>, IPlanRepository
{
    public PlanRepository(AppDbContext context, IMapper mapper) : base(context, mapper)
    {
    }
}
