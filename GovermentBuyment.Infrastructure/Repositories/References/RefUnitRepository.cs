using AutoMapper;
using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Domain.IRepositories;

namespace GovermentBuyment.Infrastructure.Repositories.References;

public class RefUnitRepository: BaseRepository<RefUnitEntity>, IRefUnitRepository
{
    public RefUnitRepository(AppDbContext context, IMapper mapper) : base(context, mapper)
    {
    }
}