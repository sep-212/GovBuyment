using AutoMapper;
using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Domain.IRepositories;

namespace GovermentBuyment.Infrastructure.Repositories.References;

public class RefTradeMethodRepository: BaseRepository<RefTradeMethodEntity>, IRefTradeMethodRepository
{
    public RefTradeMethodRepository(AppDbContext context, IMapper mapper) : base(context, mapper)
    {
    }
}