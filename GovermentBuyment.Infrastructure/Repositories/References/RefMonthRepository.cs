using AutoMapper;
using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Domain.IRepositories;

namespace GovermentBuyment.Infrastructure.Repositories.References;

public class RefMonthRepository: BaseRepository<RefMonthEntity>, IRefMonthRepository
{
    public RefMonthRepository(AppDbContext context, IMapper mapper) : base(context, mapper)
    {
    }
}