﻿using System;
using GovermentBuyment.Domain.Entities;
using GovermentBuyment.Infrastructure.Configs.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GovermentBuyment.Infrastructure.Configs;


public class PlanKatoConfig : BaseEntityConfig<PlanKatoEntity>
{
    protected override void Config(EntityTypeBuilder<PlanKatoEntity> builder)
    {
        builder.ToTable("PlanKatos", schema: "public");
        builder.OwnsOne(a => a.FullDeliveryPlace, name =>
        {
            name.Property(p => p.Ru);
            name.Property(p => p.Kk);
        });
        builder.HasOne(x => x.PlanEntity).WithMany(x => x.PlanKatos)
            .HasForeignKey(x => x.PlanPointId).HasPrincipalKey(x => x.PlanId)
            .IsRequired(true).Metadata.DeleteBehavior = DeleteBehavior.Restrict;
    }
}
