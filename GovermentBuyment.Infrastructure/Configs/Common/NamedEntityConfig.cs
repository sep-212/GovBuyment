using GovermentBuyment.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GovermentBuyment.Infrastructure.Configs.Common;



public abstract class NamedEntityConfig<TDomain> : IEntityTypeConfiguration<TDomain> where TDomain : NamedEntity
{
    protected abstract void Config(EntityTypeBuilder<TDomain> builder);

    public virtual void Configure(EntityTypeBuilder<TDomain> builder)
    {
        builder.HasKey(x => x.Id);
        builder.OwnsOne(a => a.Name, name =>
        {
            name.Property(p => p.Ru);
            name.Property(p => p.Kk);
        });       
        Config(builder);
    }
}

