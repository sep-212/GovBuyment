using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Infrastructure.Configs.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GovermentBuyment.Infrastructure.Configs.References;

public class RefMonthConfig: NamedEntityConfig<RefMonthEntity>
{
    protected override void Config(EntityTypeBuilder<RefMonthEntity> builder)
    {
        builder.ToTable("RefMonths", schema: "public");
    }
}