using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Infrastructure.Configs.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GovermentBuyment.Infrastructure.Configs.References;

public class RefUnitConfig : ReferenceEntityConfig<RefUnitEntity>
{
    protected override void Config(EntityTypeBuilder<RefUnitEntity> builder)
    {
        builder.ToTable("RefUnits", schema: "public");
    }
}