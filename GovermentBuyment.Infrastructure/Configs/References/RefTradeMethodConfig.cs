using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Infrastructure.Configs.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GovermentBuyment.Infrastructure.Configs.References;

public class RefTradeMethodConfig : ReferenceEntityConfig<RefTradeMethodEntity>
{
    protected override void Config(EntityTypeBuilder<RefTradeMethodEntity> builder)
    {
        builder.ToTable("RefTradeMethods", schema: "public");
    }
}