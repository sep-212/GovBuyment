﻿using GovermentBuyment.Domain.Entities;
using GovermentBuyment.Infrastructure.Configs.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GovermentBuyment.Infrastructure.Configs;


public class PlanConfig : BaseEntityConfig<PlanEntity>
{
    protected override void Config(EntityTypeBuilder<PlanEntity> builder)
    {
        builder.ToTable("Plans", schema: "public");
        builder.OwnsOne(a => a.Name, name =>
        {
            name.Property(p => p.Ru);
            name.Property(p => p.Kk);
        });
        builder.OwnsOne(a => a.SubjectName, name =>
        {
            name.Property(p => p.Ru);
            name.Property(p => p.Kk);
        });
        builder.OwnsOne(a => a.Description, name =>
        {
            name.Property(p => p.Ru);
            name.Property(p => p.Kk);
        });
        builder.OwnsOne(a => a.ExtraDescription, name =>
        {
            name.Property(p => p.Ru);
            name.Property(p => p.Kk);
        });

        builder.HasOne(x => x.RefTradeMethod).WithMany()
            .HasForeignKey(x => x.RefTradeMethodsId).HasPrincipalKey(x => x.SystemId)
            .IsRequired(true).Metadata.DeleteBehavior = DeleteBehavior.Restrict;
        builder.HasOne(x => x.RefUnit).WithMany()
            .HasForeignKey(x => x.RefUnitsCode).HasPrincipalKey(x => x.SystemId)
            .IsRequired(true).Metadata.DeleteBehavior = DeleteBehavior.Restrict;
        builder.HasOne(x => x.RefMonth).WithMany()
            .HasForeignKey(x => x.RefMonthId).HasPrincipalKey(x => x.SystemId)
            .IsRequired(true).Metadata.DeleteBehavior = DeleteBehavior.Restrict;
    }
}
