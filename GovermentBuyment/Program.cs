﻿using GovermentBuyment.Infrastructure;
using GovermentBuyment.RestParser.Options;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);


var connection = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<AppDbContext>(optionsBuilder =>
{
    optionsBuilder.UseNpgsql(connection);
});

builder.Services.Configure<ReferenceApiOption>(builder.Configuration.GetSection("ReferencesApi"));

builder.Services.AddControllers();
builder.Services.AddCommonServices(builder.Configuration);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    using (var scope = app.Services.CreateScope())
    {
        var context = scope.ServiceProvider.GetRequiredService<AppDbContext>();
        var migrations = await context.Database.GetPendingMigrationsAsync();
        if (migrations.ToList().Count != 0)
        {
            await context.Database.MigrateAsync();
        }
    }
}

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
