using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Domain.IRepositories;
using GovermentBuyment.Domain.SeedWork;
using GovermentBuyment.RestParser.API;
using MGZ.ParserManager.Rest;

namespace GovermentBuyment.RestParser.Handler;

public class ExtractRefTradeMethodHandler : AbstractHandler
{
    private readonly IReferencesApi _referencesApi;
    private readonly IRefTradeMethodRepository _refTradeMethodRepository;
    public ExtractRefTradeMethodHandler(IRefTradeMethodRepository refTradeMethodRepository, IReferencesApi referencesApi)
    {
        _refTradeMethodRepository = refTradeMethodRepository;
        _referencesApi = referencesApi;
    }
    public override async Task<object> Handle(object request)
    {
        try
        {
            var result = await _referencesApi.GetRefTradeMethods();
            if (result.Total == 0)
            {
                return base.Handle(request);
            }

            var items = result.Items.Select(x => new RefTradeMethodEntity()
            {
                SystemId = x.id,
                Name = new Localizable(x.name_ru, x.name_kz),
                SymbolCode = x.symbol_code,
                IsActive = x.is_active == 1,
            }).ToList();
            await _refTradeMethodRepository.AddRangeAsync(items.ToArray());
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        return base.Handle(request);
    }
}