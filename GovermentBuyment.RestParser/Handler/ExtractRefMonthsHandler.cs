using AutoMapper;
using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Domain.IRepositories;
using GovermentBuyment.Domain.SeedWork;
using GovermentBuyment.RestParser.API;
using GovermentBuyment.RestParser.DTOs;
using MGZ.ParserManager.Rest;

namespace GovermentBuyment.RestParser.Handler;

public class ExtractRefMonthsHandler : AbstractHandler
{
    private readonly IReferencesApi _referencesApi;
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    public ExtractRefMonthsHandler(IReferencesApi referencesApi, IMapper mapper, IUnitOfWork unitOfWork)
    {
        _referencesApi = referencesApi;
        _mapper = mapper;
        _unitOfWork = unitOfWork;
    }
    public override async Task<object> Handle(object request)
    {
        var result = await _referencesApi.GetRefMonths();
        if (result.Total == 0)
        {
            return base.Handle(request);
        }
        var items = _mapper.Map<List<RefMonths>, List<RefMonthEntity>>(result.Items);
        var _refMonthRepository = _unitOfWork.GetRepository<RefMonthEntity>();
        await _refMonthRepository.AddRangeAsync(items.ToArray());
        await _unitOfWork.SaveChangesAsync();
        return base.Handle(request);
    }
}