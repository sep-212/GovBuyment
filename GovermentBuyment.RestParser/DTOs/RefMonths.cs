namespace GovermentBuyment.RestParser.DTOs;

public class RefMonths
{
    public int id { get; set; }
    public string name_ru { get; set; }
    public string name_kz { get; set; }
}