using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.RestParser.DTOs;

public class RefUnits
{
    public string code { get; set; }
    public string code2 { get; set; }
    public string alpha_code { get; set; }
    public string name_ru { get; set; }
    public string name_kz { get; set; }
}