using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.RestParser.DTOs;

public class RefTradeMethod
{
    public long id { get; set; }
    public string code { get; set; }
    public string name_ru { get; set; }
    public string name_kz { get; set; }
    public string symbol_code { get; set; }
    public int is_active { get; set; }
}