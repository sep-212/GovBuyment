using AutoMapper;
using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.RestParser.DTOs;

namespace GovermentBuyment.RestParser.Mapping;

public class RefMonthMapping : Profile
{
    public RefMonthMapping()
    {
        CreateMap<RefMonthEntity, RefMonths>()
            .ForMember(x => x.id, 
                opt =>
                    opt.MapFrom(item => item.SystemId))
            .ForMember(x => x.name_ru, 
                opt => 
                    opt.MapFrom(item => item.Name.Ru))
            .ForMember(x => x.name_kz, 
                opt => 
                    opt.MapFrom(item => item.Name.Kk));
    }
}
