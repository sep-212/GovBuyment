namespace GovermentBuyment.RestParser.Options;

public class ReferenceApiOption
{
    public string ApiEndpoint { get; set; }
    public string Token { get; set; }
    public string AuthorizationScheme { get; set; }
}