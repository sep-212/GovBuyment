using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.RestParser.API;

public class ApiResult<TDomain> where TDomain: class
{
    public int Total { get; set; }
    public int Limit { get; set; }
    public string? Next_Page { get; set; }
    public List<TDomain> Items { get; set; }
}