﻿using GovermentBuyment.RestParser.DTOs;
using Refit;

namespace GovermentBuyment.RestParser.API;

public interface IReferencesApi
{
    [Get("/ref_trade_methods?limit={limit}&search_after={after}")]
    public Task<ApiResult<RefTradeMethod>> GetRefTradeMethods([AliasAs("after")] int after = 0, [AliasAs("limit")] int? limit = 2000);
    [Get("/ref_units?limit={limit}&search_after={after}")]
    public Task<ApiResult<RefUnits>> GetRefUnits([AliasAs("after")] int after = 0, [AliasAs("limit")] int? limit = 2000);
    [Get("/ref_months?limit={limit}&search_after={after}")]
    public Task<ApiResult<RefMonths>> GetRefMonths([AliasAs("after")] int after = 0, [AliasAs("limit")] int? limit = 2000);
}