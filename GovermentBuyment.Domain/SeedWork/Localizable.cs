﻿using System;
namespace GovermentBuyment.Domain.SeedWork;

public class Localizable
{
    public string? Ru { get; set; }
    public string? Kk { get; set; }
    public Localizable(string? ru, string? kk)
    {
        Ru = ru;
        Kk = kk;
    }
}
