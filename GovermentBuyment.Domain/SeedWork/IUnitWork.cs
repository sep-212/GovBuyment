namespace GovermentBuyment.Domain.SeedWork;

public interface IUnitOfWork {
    ICrudRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity, IAggregateRoot;
    Task SaveChangesAsync();
}