﻿using System;
namespace GovermentBuyment.Domain.SeedWork;

public class Entity
{
    public Guid Id { get; } = Guid.NewGuid();
    public long SystemId { get; set; }
}

public class NamedEntity : Entity
{
    public Localizable Name { get; set; }
}