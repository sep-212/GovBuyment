namespace GovermentBuyment.Domain.SeedWork;

public class ReferenceEntity : Entity
{
    public Localizable Name { get; set; }
    public string Code { get; set; }
}