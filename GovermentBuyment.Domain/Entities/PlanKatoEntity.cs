﻿using System;
using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.Domain.Entities;

public class PlanKatoEntity : Entity, IAggregateRoot
{
    public long PlanPointId { get; set; }
    public PlanEntity? PlanEntity { get; set; }
    public string? RefKatoCode { get; set; }
    public string? RefCountriesCode { get; set; }
    public Localizable FullDeliveryPlace { get; set; }
    public double? Count { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
    public int? SystemId { get; set; }
    public string? IndexDate { get; set; }
}
