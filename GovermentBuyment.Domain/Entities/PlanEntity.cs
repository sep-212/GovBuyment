﻿using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.Domain.Entities;

/// <summary>
/// Пункт плана
/// </summary>
public class PlanEntity : Entity, IAggregateRoot
{
    /// <summary>
    /// Ид плана - id
    /// </summary>
    public long PlanId { get; set; }
    public long PlanRootId { get; set; }
    public long? SystemSubjectId { get; set; }
    public long? SystemOrganizerId { get; set; }
    public string? SubjectBin { get; set; }
    public Localizable? SubjectName { get; set; }
    public Localizable? Name { get; set; }
    public long? RefMonthId { get; set; }
    public RefMonthEntity? RefMonth { get; set; }
    public long? RefTradeMethodsId { get; set; }
    public RefTradeMethodEntity? RefTradeMethod { get; set; }
    public long? RefUnitsCode { get; set; }
    public RefUnitEntity? RefUnit { get; set; }
    public long? Count { get; set; }
    public double? Price { get; set; }
    public double? Amount { get; set; }
    /// <summary>
    ///  Финансовый год - plnPointYear
    /// </summary>
    public long? FinancialYear { get; set; }
    public DateTimeOffset? DateCreate { get; set; }
    public Localizable? Description { get; set; }
    public Localizable? ExtraDescription { get; set; }
    public double? SumFirstYear { get; set; }
    public double? SumSecondYear { get; set; }
    public double? SumThirdYear { get; set; }
    public List<PlanKatoEntity> PlanKatos { get; set; }
}

