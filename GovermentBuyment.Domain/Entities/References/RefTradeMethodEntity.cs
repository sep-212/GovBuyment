using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.Domain.Entities.References;

public class RefTradeMethodEntity : ReferenceEntity, IAggregateRoot
{
    public bool IsActive { get; set; }
    public string SymbolCode { get; set; }
}