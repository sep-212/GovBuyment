using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.Domain.Entities.References;

public class RefUnitEntity : ReferenceEntity, IAggregateRoot
{
    public string AlphaCode { get; set; }
}