using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.Domain.IRepositories;

public interface IRefTradeMethodRepository : ICrudRepository<RefTradeMethodEntity>
{
    
}