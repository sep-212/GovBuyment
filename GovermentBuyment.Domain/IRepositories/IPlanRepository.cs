﻿using System;
using GovermentBuyment.Domain.Entities;
using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.Domain.IRepositories;

public interface IPlanRepository : ICrudRepository<PlanEntity>
{
}

