using GovermentBuyment.Domain.Entities.References;
using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.Domain.IRepositories;

public interface IRefUnitRepository : ICrudRepository<RefUnitEntity>
{
    
}