using GovermentBuyment.Domain.Entities;
using GovermentBuyment.Domain.SeedWork;

namespace GovermentBuyment.Domain.IRepositories;

public interface IPlanKatoRepository: ICrudRepository<PlanKatoEntity>
{
}
